# tooz iOS-SDK-Examples

## 1. Introduction

This app showcases the use of the tooz iOS-SDK through two example apps.

1.) Simple Examples that showcase the following features:  

* **Sending an UIImageView to the glasses**
* **Sending a UITextView to the glasses**
* **Receiving sensor events** (Note: The emulator only supports accelerometer and gyroscope sensors)
* **Receiving button events**

2.) A music player app that showcases the use of this SDK while the app is in background. This app continuously sends the currently played track and the elapsed track time to the glasses.

## 2. Getting started

Clone this repository, open the desired Xcode-project, adapt the signing profile by changing the _Bundle Identifier_ and start the app on a physical device. Use either the real glasses or the emulator to pair with the app. You do not need to pair with the glasses or the emulator manually through the settings menu of the phone; rather the example app will leverage the SDK to pair programmatically.

## 3. Troubleshooting

If any problem should arise, feel free to contact us at <dev@tooztech.com>. 

**Note**: The emulator does not emulate all the functionality of the tooz glasses and **it is much slower** - images take longer to be received by the emulator than by the physical glasses. Therefore it is strongly advised to not release an app to production that was not tested with the physcial glasses.