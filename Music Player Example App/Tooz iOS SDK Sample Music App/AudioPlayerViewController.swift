import UIKit
import AVFoundation

class AudioPlayerViewController: BaseViewController, AVAudioPlayerDelegate {
    
    private let defaultPlayerVolume: Float = 0.5
    private let playButton = UIButton()
    private var displayLink: CADisplayLink?
    private var currentPlayTime: String?

    public var position: Int = 0
    public var songs: [Song] = []
    
    @IBOutlet var container: UIView!
    
    var player: AVAudioPlayer?
    
    private let albumImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let songNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica-Bold", size: 28)
        return label
    }()
    
    private let artistNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica", size: 24)
        return label
    }()
    
    private let timeView: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica", size: 24)
        return label
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if container.subviews.count == 0 {
            setupUi()
            play()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let player = player {
            player.stop()
            displayLink?.isPaused = true
        }
        if let app = UIApplication.shared.delegate as? AppDelegate {
            app.toozImageTimer?.invalidate()
        }
    }
    
    private func setupUi() {
        let song = songs[position]
        let labelHeight: CGFloat = 50
        
        albumImageView.frame = CGRect(x: 0, y: 0, width: container.frame.size.width, height: container.frame.size.width)
        albumImageView.image = UIImage(named: song.image)
        songNameLabel.frame = CGRect(x: 10, y: albumImageView.frame.size.height + 10, width: container.frame.size.width - 20, height: labelHeight)
        artistNameLabel.frame = CGRect(x: 10, y: albumImageView.frame.size.height + 10 + labelHeight, width: container.frame.size.width - 20, height: labelHeight)
        songNameLabel.text = song.name
        artistNameLabel.text = song.artist
        container.addSubview(albumImageView)
        container.addSubview(songNameLabel)
        container.addSubview(artistNameLabel)
        
        let nextButton = UIButton()
        let backButon = UIButton()
        
        playButton.setBackgroundImage(UIImage(systemName: "pause.fill"), for: .normal)
        nextButton.setBackgroundImage(UIImage(systemName: "forward.fill"), for: .normal)
        backButon.setBackgroundImage(UIImage(systemName: "backward.fill"), for: .normal)
        
        let controlsYPosition = artistNameLabel.frame.origin.y + labelHeight + 20
        let controlsSize: CGFloat = 40
        
        playButton.frame = CGRect(x: (container.frame.width - controlsSize) / 2.0, y: controlsYPosition, width: controlsSize, height: controlsSize)
        nextButton.frame = CGRect(x: container.frame.width - (controlsSize * 2), y: controlsYPosition, width: controlsSize, height: controlsSize)
        backButon.frame = CGRect(x: (controlsSize), y: controlsYPosition, width: controlsSize, height: controlsSize)
        
        playButton.tintColor = .black
        
        if isFirstSong() {
            backButon.tintColor = .gray
        } else {
            backButon.tintColor = .black
        }
        if isLastSong() {
            nextButton.tintColor = .gray
        } else {
            nextButton.tintColor = .black
        }
        
        playButton.addTarget(self, action: #selector(onPlayClicked(_:)), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(onNextClicked(_:)), for: .touchUpInside)
        backButon.addTarget(self, action: #selector(onBackClicked(_:)), for: .touchUpInside)
        
        container.addSubview(playButton)
        container.addSubview(nextButton)
        container.addSubview(backButon)
        
        let sliderHeight: CGFloat = 10
        let slider = UISlider(frame: CGRect(x: 20, y: container.frame.size.height - 60, width: container.frame.size.width - 40, height: sliderHeight))
        slider.value = defaultPlayerVolume
        slider.addTarget(self, action: #selector(onSliding(_:)), for: .valueChanged)
        container.addSubview(slider)
        
        let timeViewWidth: CGFloat = 100
        timeView.frame = CGRect(x: (container.frame.width - timeViewWidth) / 2.0, y: container.frame.size.height - sliderHeight - 140, width: timeViewWidth, height: 30)
        container.addSubview(timeView)
        
        displayLink = CADisplayLink(target: self, selector: #selector(updateTimeView))
        displayLink?.add(to: .current, forMode: RunLoop.Mode.default)
        displayLink?.isPaused = true
    }
    
    private func play() {
        let session = AVAudioSession.sharedInstance()

        do {
            try session.setCategory(.playback, mode: .default, policy: .longFormAudio, options: [])
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            
            let song = songs[position]
            
            guard
                let urlString = Bundle.main.path(forResource: song.track, ofType: "mp3")
            else {
                return
            }
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: urlString))
            guard let player = player else {
                return
            }
            player.delegate = self
            player.volume = defaultPlayerVolume
            player.play()
            displayLink?.isPaused = false
            
            guard let app = UIApplication.shared.delegate as? AppDelegate else { return }
            
            app.toozImageTimer?.invalidate()
            app.toozImageTimer = Timer(fire: Date(), interval: 1, repeats: true, block: { [ weak self ] toozImageTimer in
                guard let self = self, let toozView = self.getToozView() else { return }
                app.tooz.sendView(view: toozView, timeToLive: 3000, compressionQuality: 0.1)
            })
            RunLoop.current.add(app.toozImageTimer!, forMode: RunLoop.Mode.default)
        
        } catch {
            print("error occured setting up audio player: \(error)")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if !isLastSong() {
            updateUiAndPlaySong(SongDirection.next)
        } else {
            updateUiAndPlaySong(SongDirection.restart)
        }
    }
    
    @objc func updateTimeView() {
        guard let player = player else { return }
        let currentTime = Int(player.currentTime)
        let minutes = currentTime / 60
        let seconds = currentTime - minutes * 60
        
        if minutes > 0 {
            currentPlayTime = String("\(minutes)m \(seconds)s")
            
        } else {
            currentPlayTime = String("\(seconds)s") as String
        }
        timeView.text = currentPlayTime
    }
    
    @objc func onPlayClicked(_ button: UIButton) {
        if player?.isPlaying == true {
            player?.pause()
            displayLink?.isPaused = true
            playButton.setBackgroundImage(UIImage(systemName: "play.fill"), for: .normal)
        } else {
            player?.play()
            displayLink?.isPaused = false
            playButton.setBackgroundImage(UIImage(systemName: "pause.fill"), for: .normal)
        }
    }
    
    @objc func onNextClicked(_ button: UIButton) {
        if !isLastSong()  {
            player?.stop()
            displayLink?.isPaused = true
            updateUiAndPlaySong(SongDirection.next)
        }
    }
    
    @objc func onBackClicked(_ button: UIButton) {
        if !isFirstSong() {
            player?.stop()
            displayLink?.isPaused = true
            updateUiAndPlaySong(SongDirection.previous)
        }
    }
    
    @objc func onSliding(_ slider: UISlider) {
        let value = slider.value
        print("VOLUME: \(value)")
        player?.volume = value
        // adjust player volume
    }
    
    private func isFirstSong() -> Bool {
        return position == 0
    }

    private func isLastSong() -> Bool {
        return position == songs.count - 1
    }
    
    private func updateUiAndPlaySong(_ songDirection: SongDirection) {
        switch songDirection {
        case .next:
            position += 1
        case .previous:
            position -= 1
        case .restart:
            position = 0
        }
        for subView in container.subviews {
            subView.removeFromSuperview()
        }
        setupUi()
        play()
    }
    
    private func getToozView() -> UIView? {
        // Views must exactly be 400 x 640 px, this is the resolution of the glasses.
        let textView = UITextView(frame: .init(x: 0, y: 0, width: 400 / UIScreen.main.scale, height: 640 / UIScreen.main.scale))
        textView.textColor = UIColor(ciColor: .white)
        textView.backgroundColor = UIColor(ciColor: .black)
        textView.textAlignment = NSTextAlignment.center
        textView.font = UIFont(name: "Helvetica-Bold", size: 28)
        textView.text = "\n\n\nPlaying\n\(songs[position].name)\n\n\(currentPlayTime ?? "")"
        return textView
    }
    
    internal enum SongDirection {
        case next
        case previous
        case restart
    }
}
