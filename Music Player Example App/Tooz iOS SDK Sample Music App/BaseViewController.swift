import Foundation
import UIKit

protocol PeerRemovedPairingInformationListener {
    func onPeerRemovedPairingInformation()
}

class BaseViewController : UIViewController, PeerRemovedPairingInformationListener {
    
    private let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
        
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }

        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)")
            })
        }
    }
    
    func onPeerRemovedPairingInformation() {
        let alert = UIAlertController(title: "Action needed", message: "An error has occured. The tooz device must manually be forgotten from the bluetooth settings in order to work again.", preferredStyle: .alert)
        
        alert.addAction(settingsAction)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
}
