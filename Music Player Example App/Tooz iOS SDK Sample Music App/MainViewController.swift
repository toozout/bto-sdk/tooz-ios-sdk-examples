import UIKit

class MainViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var songs = [Song]()
    
    @IBOutlet var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSongs()
        table.delegate = self
        table.dataSource = self
    }
    
    func addSongs() {
        songs.append(Song(name: "Hazy After Hours", artist: "Alejandro Magaña (A. M.)", image: "happyrock", track: "mixkit-hazy-after-hours-132"))
        songs.append(Song(name: "Life Is A Dream", artist: "Michael Ramir C.", image: "jazzyfrenchy", track: "mixkit-life-is-a-dream-837"))
        songs.append(Song(name: "Tech House Vibes", artist: "Alejandro Magaña (A. M.)", image: "littleidea", track: "mixkit-tech-house-vibes-130"))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let song = songs[indexPath.row]
        cell.textLabel?.text = song.name
        cell.detailTextLabel?.text = song.artist
        cell.imageView?.image = UIImage(named: song.image)
        cell.textLabel?.font = UIFont(name: "Helvetica-Bold", size: 18)
        cell.detailTextLabel?.font = UIFont(name: "Helvetica", size: 17)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let position = indexPath.row
        guard let audioPlayerViewController = storyboard?.instantiateViewController(identifier: "audio_player") as? AudioPlayerViewController else {
            return
        }
        audioPlayerViewController.songs = songs
        audioPlayerViewController.position = position
        present(audioPlayerViewController, animated: true)
    }
    
}

struct Song {
    let name: String
    let artist: String
    let image: String
    let track: String
    
}
