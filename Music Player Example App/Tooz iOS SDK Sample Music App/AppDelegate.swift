import UIKit
import tooz_iOS_SDK
import CoreBluetooth

@main
class AppDelegate: UIResponder, UIApplicationDelegate, ConnectionStateListener, ScanListener {
   
    private var isConnectedoTooz = false
    lazy private(set) var tooz: ToozBluetooth = Tooz.shared()
    var toozImageTimer: Timer?
    
    // In a production app one would normally save all found devices in an array and let the user choose his device
    private var peripheral: CBPeripheral? = nil
    
    override init() {
        super.init()
        tooz.setConnectivityListener(scanListener: self, connectionStateListener: self)
    }

    func onConnectionStateChanged(connectionState: ConnectionState) {
        
        switch connectionState {
        case .connected:
            print("connectionState: connected")
            isConnectedoTooz = true
        case .disconnected:
            print("connectionState: disconnected")
            isConnectedoTooz = false
        case .connectionError(cause: let cause):
            print("connectionState: connectionError \(cause as Optional)")
            isConnectedoTooz = false
            Tooz.reset()
            if let cause = cause {
                handleConnectionError(cause: cause)
            } else {
                tooz.scan(periodic: true)
            }
        case .bluetoothReady:
            tooz.scan(periodic: true)
        @unknown default:
            print("Unkown connetion state")
        }
    }
    
    func onDeviceFound(peripheral: CBPeripheral) {
        self.peripheral = peripheral
        connectToTooz()
    }
    
    func sendView(view: UIView) {
        if isConnectedoTooz {
            // Setting timeToLive to nil means an indefinite amount of time
            tooz.sendView(view: view, timeToLive: nil, compressionQuality: 0.1)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func connectToTooz() {
        if !isConnectedoTooz {
            if let peripheral = peripheral {
                tooz.stopScan()
                tooz.connect(peripheral: peripheral)
            } else {
                tooz.scan(periodic: true)
            }
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        toozImageTimer?.invalidate()
    }
    
    private func handleConnectionError(cause: ConnectionErrorCause) {
        if cause == ConnectionErrorCause.peerRemovedPairingInformation {
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                if let baseController = ((topController as? UINavigationController)?.topViewController) as? BaseViewController {
                    baseController.onPeerRemovedPairingInformation()
                }
            }
        } else {
            tooz.scan(periodic: true)
        }
    }

}
