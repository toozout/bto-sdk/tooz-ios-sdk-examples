import SwiftUI

struct ExamplesView: View {
    
    @EnvironmentObject var tooz: ToozObservable
    
    var body: some View {
        NavigationView() {
            List {
                NavigationLink(destination: ImageListView().environmentObject(tooz)) {
                    Text("Image Example")
                }
                NavigationLink(destination: TextViewExampleDetailView().environmentObject(tooz)) {
                    Text("TextView Example")
                }
                NavigationLink(destination: ButtonEventListView().environmentObject(tooz)) {
                    Text("Button Event Listener")
                }
                NavigationLink(destination: SensorEventView().environmentObject(tooz)) {
                    Text("Sensor Event Listener")
                }
            }
            .navigationBarTitle(Text("Examples"))
        }
        .alert(isPresented: self.$tooz.showPeerRemovedPairingInfoError, content: {
            Alert(title: Text("Alert"), message: Text("An internal bluetooth error occured. Please remove the tooz device in bluetooth system settings and try again"), dismissButton: .default(Text("Ok")))
        })
    }
    
}

struct ExamplesView_Previews: PreviewProvider {
    static var previews: some View {
        ExamplesView()
    }
}
