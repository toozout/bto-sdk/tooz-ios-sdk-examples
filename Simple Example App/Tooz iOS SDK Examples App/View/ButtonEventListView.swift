import SwiftUI

struct ButtonEventListView: View {
    
    @EnvironmentObject var tooz: ToozObservable
    
    var body: some View {
        List(){
            ForEach(tooz.buttonCodes.map { ButtonListModel(text: "Button code: \($0)") }) { item in
                Text(item.text)
            }
        }
    }
}

struct ButtonListModel: Identifiable, Hashable {
    var id = UUID()
    var text: String
}

struct ButtonEventListView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonEventListView()
    }
}
