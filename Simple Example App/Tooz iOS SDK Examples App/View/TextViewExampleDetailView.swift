import SwiftUI

struct TextViewExampleDetailView: View {
    
    @EnvironmentObject var tooz: ToozObservable
    
    var body: some View {
        VStack(alignment: .center) {
            Text("Send a simple textview too the glasses. The textview has no connection to the views visible on screen and must always have a size of 400x640 px (not points).")
                .font(.subheadline)
           Button(action: {
                if (tooz.isConnected) {
                    // Views must exactly be 400 x 640 px, this is the resolution of the glasses.
                    let textView = UITextView(frame: .init(x: 0, y: 0, width: 400 / UIScreen.main.scale, height: 640 / UIScreen.main.scale))
                    textView.textColor = UIColor(.black)
                    textView.textAlignment = NSTextAlignment.center
                    textView.text = "This is one awesome textview"
                    tooz.toozBluetooth?.sendView(view: textView, timeToLive: nil, compressionQuality: 0.5)
                } else {
                    tooz.connect()
                }
            }) {
                if (tooz.isConnected) {
                    Text("Send TextView")
                } else {
                    Text("Connect to tooz")
                }
            }
            .frame(width: Constants.buttonWidth, height: Constants.buttonHeight, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .padding()
        }
        .padding()
    }
}

struct TextViewExample_Previews: PreviewProvider {
    static var previews: some View {
        TextViewExampleDetailView()
    }
}
