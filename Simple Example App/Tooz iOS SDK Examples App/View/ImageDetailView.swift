import SwiftUI
import Foundation

struct ImageDetailView: View {
    
    var imageResource: String
    @EnvironmentObject var tooz: ToozObservable
    
    var body: some View {
        VStack(alignment: .center) {
            Image(imageResource)
                .resizable()
                .aspectRatio(0.625, contentMode: .fill)
                .padding(EdgeInsets(top: 0, leading: 60, bottom: 60, trailing: 60))
            Button(action: {
                if (tooz.isConnected) {
                    // Views must exactly be 400 x 640 px, this is the resolution of the glasses.
                    let imageView = UIImageView(frame: .init(x: 0, y: 0, width: 400 / UIScreen.main.scale, height: 640 / UIScreen.main.scale))
                    imageView.image = UIImage(named: imageResource)
                    tooz.toozBluetooth?.sendView(view: imageView, timeToLive: nil, compressionQuality: 0.1)
                } else {
                    tooz.connect()
                }
            }) {
                if (tooz.isConnected) {
                    Text("Send Image")
                } else {
                    Text("Connect to tooz")
                }
            }
            .frame(width: Constants.buttonWidth, height: Constants.buttonHeight)
        }
        .padding(.bottom, 60)
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ImageDetailView(imageResource: "turtleRock")
    }
}
