import SwiftUI

private let spacing: CGFloat = 16

struct ImageListView: View {
    
    @EnvironmentObject var tooz: ToozObservable
    
    let data = Array(0...19).map { "\($0)_travel" }
    
    let layout = [
        GridItem(.flexible(minimum: 40), spacing: spacing),
        GridItem(.flexible(minimum: 40), spacing: spacing),
    ]
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: layout, spacing: spacing) {
                ForEach(data, id: \.self) { item in
                    NavigationLink(destination: ImageDetailView(imageResource: item)) {
                        Image(item)
                            .resizable()
                            .aspectRatio(0.625, contentMode: .fill)
                    }
                }
            }
            .padding(.horizontal)
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        ImageListView()
    }
}
