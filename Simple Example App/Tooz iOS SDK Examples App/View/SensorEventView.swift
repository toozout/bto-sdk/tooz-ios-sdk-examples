import SwiftUI
import tooz_iOS_SDK

struct SensorEventView: View, SensorDataListener {
    
    @EnvironmentObject var tooz: ToozObservable
    
    @State var isRegisteredForSensorEvents = false
    @State var sensorReading: SensorReading? = nil
    
    func onSensorDataReceived(_ sensors: [Sensor]) {
        sensors.forEach { sensor in
            if sensor.name == SensorType.acceleration.rawValue {
                sensorReading = sensor.reading
            }
        }
    }
    

    var body: some View {
        VStack {
            if sensorReading != nil {
                VStack {
                    Text("x: \(sensorReading!.acceleration?.x?.string ?? "")")
                    Text("y: \(sensorReading!.acceleration?.y?.string ?? "")")
                    Text("z: \(sensorReading!.acceleration?.z?.string ?? "")")
                }
            }
            
            Button(action: {
                if tooz.isConnected {
                    if isRegisteredForSensorEvents {
                        isRegisteredForSensorEvents = false
                        deregisterFromSensorEvents()
                    } else {
                        isRegisteredForSensorEvents = true
                        registerForSensorEvents()
                    }
                } else {
                    tooz.connect()
                }
            }) {
                if tooz.isConnected {
                    if isRegisteredForSensorEvents {
                        Text("Deregister from sensor events")
                    } else {
                        Text("Register for sensor events")
                    }
                } else {
                    Text("Connect to tooz")
                }
            }
            .frame(width: Constants.buttonWidth, height: Constants.buttonHeight, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .padding()
        }
        .onDisappear {
            deregisterFromSensorEvents()
        }
    }
    
    private func registerForSensorEvents() {
        // We want to listen for the acceleration, the values should be sent with an interval of 500 ms. It is possible to add multiple sensors to this array
        // Note that with the emulator, only acceleration and gyroscope are supported.
        tooz.toozBluetooth?.setSensorDataListener(sensorDataListener: self, sensorsOfInterest: [SensorOfInterest(name: SensorType.acceleration, delay: 500)])
    }

    
    private func deregisterFromSensorEvents() {
        tooz.toozBluetooth?.setSensorDataListener(sensorDataListener: nil, sensorsOfInterest: nil)
    }
}

struct SensorModel: Identifiable {
    let id = UUID()
    let sensor: Sensor
    
    init(sensor: Sensor) {
        self.sensor = sensor
    }
}

struct SensorEventItemView: View {
    
    var text = ""
    
    var body: some View {
        Text(text)
            .padding()
    }
}

struct SensorEventListModel: Identifiable, Hashable {
    var id = UUID()
    var text: String
}

struct SensorEventListView_Previews: PreviewProvider {
    static var previews: some View {
        SensorEventView()
    }
}

extension LosslessStringConvertible {
    var string: String { .init(self) }
}
