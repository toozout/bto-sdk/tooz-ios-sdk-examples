import SwiftUI
import tooz_iOS_SDK
import CoreBluetooth

@main
struct ToozTestApp: App {
        
    @Environment(\.scenePhase) private var scenePhase
    
    var tooz = ToozObservable()
    
    var body: some Scene {
        WindowGroup {
            ExamplesView().environmentObject(tooz)
        }
        .onChange(of: scenePhase) { phase in
            switch phase {
            case .background:
                print("background lifecycle")
            case .inactive:
                print("inactive lifecycle")
            case .active:
                print("active lifecycle")
            @unknown default:
                print("unknown lifecycle")
            }
        }
    }
}

class ToozObservable: ObservableObject, ConnectionStateListener, ButtonEventListener, ScanListener {
       
    @Published var showPeerRemovedPairingInfoError = false
    @Published var isConnected = false
    @Published var toozBluetooth: ToozBluetooth?
    @Published var buttonCodes: [ButtonCode] = []
    
    // In a production app one would normally save all found devices in an array and let the user choose his device
    private var peripheral: CBPeripheral? = nil
    
    func onConnectionStateChanged(connectionState: ConnectionState) {
        print("onConnectionStateChanged \(connectionState)")
        switch connectionState {
        case .connected:
            isConnected = true
            showPeerRemovedPairingInfoError = false
            toozBluetooth?.setButtonEventListener(self)
        case .disconnected:
            isConnected = false
            showPeerRemovedPairingInfoError = false
        case .connectionError(let cause):
            isConnected = false
            Tooz.reset()
            if cause == ConnectionErrorCause.peerRemovedPairingInformation {
                showPeerRemovedPairingInfoError = true
            } else {
                showPeerRemovedPairingInfoError = false
            }
            toozBluetooth?.scan(periodic: true)
        case .bluetoothReady:
            toozBluetooth?.scan(periodic: true)
        @unknown default:
            fatalError("This could not have happened")
        }
    }
    
    func onDeviceFound(peripheral: CBPeripheral) {
        self.peripheral = peripheral
        connect()
    }
    
    func onButtonEventReceived(_ buttonCode: ButtonCode) {
        buttonCodes.insert(buttonCode, at: 0)
    }
    
    func connect() {
        if let peripheral = peripheral {
            toozBluetooth?.stopScan()
            toozBluetooth?.connect(peripheral: peripheral)
        } else {
            toozBluetooth?.scan(periodic: true)
        }
    }
    
    init() {
        toozBluetooth = Tooz.shared()
        toozBluetooth?.setConnectivityListener(scanListener: self, connectionStateListener: self)
        toozBluetooth?.scan(periodic: true)
    }
    
}

struct Constants {
    static let buttonWidth: CGFloat = 200
    static let buttonHeight: CGFloat = 30
}
